<?php

/**
 * This File is part of the Monolog package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Monolog;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\Package\Package;
use \Selene\Module\Package\ExportResourceInterface;
use \Selene\Module\Package\FileRepositoryInterface;
use \Selene\Package\Monolog\Process\ReplaceLogger;
use \Selene\Module\DI\Processor\ProcessorInterface;

/**
 * @class MonologPackage
 * @package Monolog
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class MonologPackage extends Package implements ExportResourceInterface
{
    /**
     * {@inheritdoc}
     */
    public function build(BuilderInterface $builder)
    {
        //$builder->getProcessor()
        //    ->add(new ReplaceLogger, ProcessorInterface::AFTER_REMOVE);
    }

    /**
     * {@inheritdoc}
     */
    public function getExports(FileRepositoryInterface $files)
    {
        $files->createTarget($this->getResourcePath().'/publish/config.xml');
    }
}
