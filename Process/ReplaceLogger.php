<?php

/**
 * This File is part of the Selene\Package\Monolog package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Monolog\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Reference;
use \Selene\Module\DI\Definition\DefinitionInterface;
use \Selene\Module\DI\Processor\ProcessInterface;
use \Selene\Module\DI\Processor\ProcessorInterface;

/**
 * @class ReplaceLogger
 * @package Selene\Package\Monolog
 * @version $Id$
 */
class ReplaceLogger implements ProcessInterface
{
    public function process(ContainerInterface $container)
    {
        if (!$container->hasDefinition('monolog')) {
            return;
        }

        $this->container = $container;


        foreach ($container->getDefinitions() as $definition) {
            $this->replaceLogger($definition);
        }
    }

    private function replaceLogger(DefinitionInterface $definition)
    {
        foreach ($definition->getArguments() as $index => $argument) {
            if ($argument instanceof Reference && 'logger' === (string)$argument) {
                $definition->replaceArgument(new Reference('monolog'), $index);
            }
        }

        $this->replaceLoggerInSetters($definition);
    }

    private function replaceLoggerInSetters(DefinitionInterface $definition)
    {
        if (!$definition->hasSetters()) {
            return;
        }
    }
}
